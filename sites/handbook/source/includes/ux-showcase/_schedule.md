[//]: # TIP: Create the schedule in a temporary spreadsheet, and then copy/paste the rows into an online markdown generator (https://www.google.com/search?q=copy-table-in-excel-and-paste-as-a-markdown-table)

| Date       | Host                        | Presenter 1     | Presenter 2     | Presenter 3      | 
| ---------- | --------------------------- | --------------- | --------------- | ---------------- | 
| 2022-10-26 | Jacki Bauer                 | Becka Lippert   | Jeremy Elder    | Andy Volpe       | 
| 2022-11-09 | Justin Mandell              | Matej Latin     | Kevin Comoli    | Ali Ndlovu       | 
| 2022-11-23 | Taurie Davis                | Matt Nearents   | Nick Brandt     | Libor Vanc       | 
| 2022-12-07 | TBD (APAC)                  | Katie Macoy     |                 |                  | 
| 2022-12-21 | Marcel van Remmerden        |                 |                 |                  | 
